# ReadME.md

This is a readme file for the applications used in the thesis.
The codes are to be run in two ways, everything goes in "only" Host FS is app 2 and the rest is app 1.

# 1. Prerequisities

## 1.1 Required Software
The code was tested with,
 - Python 3.6
 - Hadoop 2.9.2
 - Os (Ubuntu 16.04, 18.04)
 - JDK 1.8

## 1.2 Path Variables
Python code is running under the condition that Hadoop's bin directory is in $PATH variable. This is also a must for Hadoop to be able to run.
Check hadoop commands that they work from any directory.
in terminal
    $hdfs will print all the commands with descriptions. General usage is :


Usage hdfs --config confdir --loglevel loglevel COMMAND

    where COMMAND is one of:
    classpath
    dfs
    fetchdt
    fsck
    getconf
    groups
    lsSnapshottableDir
    ...

    ## Create files and

## 1.3 Setting up a Hadoop Cluster

Setting up a hadoop cluster in different environments is explained in the official page.
[http://hadoop.apache.org/docs/stable/hadoop-project-dist/hadoop-common/SingleCluster.html](http://hadoop.apache.org/docs/stable/hadoop-project-dist/hadoop-common/SingleCluster.html)


# 2. Running Scripts

For Python scripts (All of the Test 2 cases and Test 1 cases related to HDFS)

    $ python3.6 hdfsbench.py hdfs_tasks.json -r 2
where

> hdfsbench$.$py : main script
> hdfs_tasks.json : Test list in JSON format
> -r N : repeat the test N times

> Test List:
● hdfs_tasks.json : HDFS operation test
●	fs_tasks.json: Host FS operation test
●	map_reduce.json: Map Reduce test
●	wordcount.json: Word count test
>
 Json Content
> ●	name: Simple test title for display
> ●	command: one of copy, delete, move, mapReduce, wordCount
> ●	source: source file or directory.
> ●	hdfs:  file or directory in HDFS
> ●	host: file or directory in host file system.
> ●	destination: destination file or directory.
> ●	hdfs: file or directory in HDFS
> ●	host: file or directory in host file system.

Source and Destination directories are configered in config.py
A working example for migration between adir and bdir for both files systems would be :

    hdfs_source_dir = "/adir"
    hdfs_dest_dir = "/bdir"
    fs_source_dir = "./adir"
    fs_dest_dir = "./bdir"

For running the Java test code,

in the directory below command shows how to use :

    ## java -jar copier.jar (optional -h for help)

Necessary arguments are :

    -s <location of file to be copied>
    -d <location of directory where file is to be copied>
    -r <number of times to repeat>
    A working example for creating 5 mirrors of test.zip would be
    Java -jar copier.jar -s usr/bin/test.zip -d usr/home/desktop -r 5

For tests in HDFS we need to create directories and put some files in them.
To do so,

> ●	/adir : It will be used as a source directory
> ●	/bdir : It will be used as a destination directory

    $ hdfs dfs -mkdir /adir
    $ hdfs dfs -mkdir /bdir
    $ hdfs dfs -ls /
    Found 2 items
    drwxr-xr-x   - edz supergroup          0 2019-06-21 16:41 /adir
    drwxr-xr-x   - edz supergroup          0 2019-06-21 16:41 /bdir

We need to put some files into the directories to be able to run the tests. To copy the whole Hadoop folder for example,

    $ hdfs dfs -put etc/hadoop/* /adir/
Then we can check with ls

    $ hdfs dfs -ls /adir
    Found 29 items
    -rw-r--r--   1 edz supergroup       7861 2019-06-21 16:58 /adir/capacity-scheduler.xml
    -rw-r--r--   1 edz supergroup       1335 2019-06-21 16:58 /adir/configuration.xsl
    -rw-r--r--   1 edz supergroup       1211 2019-06-21 16:58 /adir/container-executor.cfg
    -rw-r--r--   1 edz supergroup        881 2019-06-21 16:58 /adir/core-site.xml

We need to do the same for the Host FS, too.

> Create two directories for tests:

    $ mkdir source
    $ mkdir dest
    $ ls
    a.zip b.zip c.txt __list of files__

> Similar example to Hadoop code, to copy the Hadoop folder and what is inside to the source directory:

    Copy files to ./source for file copy test
    $ cp ~/hadoop-2.9.2/etc/hadoop/* source/
    $ ls source/
    capacity-scheduler.xml  hadoop-env.sh		httpfs-env.sh		
    kms-env.sh		mapred-env.sh		ssl-server.xml.example		configuration.xsl       
    hadoop-metrics2.properties		httpfs-log4j.properties		kms		log4j.properties  
    mapred-queues.xml.template		yarn-env.cmd container-executor.cfg		hadoop-metrics.properties   
    httpfs-signature.secret		kms-site.xml		mapred-site.xml.template		
    yarn-env.sh core-site.xml		hadoop-policy.xml		httpfs-site.xml          
    log4j.properties		slaves		yarn-site.xml hadoop-env.cmd		hdfs-site.xml               
    kms-acls.xml		mapred-env.cmd		ssl-client.xml.example



## Working Examples

Hdfs to Hdfs file migration

> config$.$py

    hdfs_source_dir = "/adir"
    hdfs_dest_dir = "/bdir"
    fs_source_dir = "./source"
    fs_dest_dir = "./dest"

> hdfs_copy_file.json



		[
		{
		"name": "Copy Paste selected file from hdfs to hdfs.",
        "command": "copy",
        "source": "hdfs:filea.txt",
        "destination": "hdfs:"
        }
        ]

inside the python directory, to run the test 20 times;

    python hdfsbench.py hdfs_copy_file.json -r 20

# Running PowerAPI

After compiling powerAPI, it needs a few arguments to run with.

> ●	--apps: With application name
> ○	For java application: --apps java
> ○	For python, java: --apps java,chrome
> ●	--pids: Process Ids
> ○	--pids 16702,16890
> ●	--agg: Aggregation function
> ○	max, sum, mean, median, stdev
> ●	duration: duration in seconds. The measurement will last forever without this option.
>

> To run a measurement:

First, in a new terminal window, go to the powerapi cli directory,

    cd powerapi-cli/

Then run the script with this bash command (monitors java applications for 30 seconds with a 1000ms frequency (once a second)

    ./bin/powerapi modules procfs-cpu-simple monitor --frequency 1000 --apps java --agg max --console duration 30

To send the output to a file output.txt, append ">>" as following

    ./bin/powerapi modules procfs-cpu-simple monitor --frequency 1000 --apps java --agg max --console duration 30 >> output.txt
