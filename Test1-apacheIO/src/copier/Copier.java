/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package copier;

import com.beust.jcommander.JCommander;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

/**
 *
 * @author edd
 */
public class Copier {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Args arg = new Args();
        JCommander commander = new JCommander();

        commander.addObject(arg);
        commander.parse(args);

        if (arg.help) {
            commander.usage();
        }

        if (arg.dest != null && arg.source != null && arg.repeate == 0) {

            if (arg.dest.canRead() && arg.source.canRead()) {

                String fileName = FilenameUtils.removeExtension(arg.source.getName());
                File copydir = new File(arg.dest.getAbsolutePath() + "/" + fileName);

                copydir.mkdir();

                try {
                    System.out.println("\tCopying file " + arg.source.getName() + " to: " + copydir.getAbsolutePath());
                    FileUtils.copyFileToDirectory(arg.source, new File(copydir.getAbsolutePath()));
                    System.out.println("\tFile Sucessfuly copied!");
                } catch (IOException ex) {
                    System.err.println("\tERROR: Could not parse given paths");
                }
            } else {
                System.err.println("\tERROR: Could not parse given paths");
            }

        } else if (arg.dest != null && arg.source != null && arg.repeate > 0) {

            if (arg.dest.canRead() && arg.source.canRead()) {

                String fileName = FilenameUtils.removeExtension(arg.source.getName());

                for (int i = 1; i <= arg.repeate; i++) {

                    File copydir = new File(arg.dest.getAbsolutePath() + "/" + fileName + i);

                    copydir.mkdir();

                    try {
                        System.out.println("\tCopying file " + arg.source.getName() + " to: " + copydir.getAbsolutePath());
                        FileUtils.copyFileToDirectory(arg.source, new File(copydir.getAbsolutePath()));
                        System.out.println("\tFiles Sucessfuly copied!");
                    } catch (IOException ex) {
                        System.err.println("\tERROR: Could not parse given paths");
                    }
                }
            } else {
                System.err.println("\tERROR: Could not parse given paths");
            }

        } else if (arg.dest == null && arg.source == null && arg.repeate == 0 && arg.sdir != null && arg.ddir != null) {
            if (arg.sdir.canRead()) {

                try {
                    System.out.println("\tCopying directory: " + arg.sdir.getName() + " to: " + arg.ddir.getAbsolutePath());
                    arg.ddir.mkdir();
                    FileUtils.copyDirectory(arg.sdir, arg.ddir);
                    System.out.println("\tDirectory Sucessfuly Copied!");
                } catch (Exception e) {
                    System.err.println("\tERROR: Could not parse given paths");
                }

            } else {
                System.err.println("\tERROR: Could not parse given paths");
            }
        } else if (arg.dest == null && arg.source == null && arg.repeate > 0 && arg.sdir != null && arg.ddir != null) {
            if (arg.sdir.canRead()) {

                try {
                    for (int i = 1; i <= arg.repeate; i++) {
                        File copydir = new File(arg.ddir.getAbsolutePath() + i);

                        copydir.mkdir();

                        System.out.println("\tCopying directory: " + arg.sdir.getName() + " to: " + copydir.getAbsolutePath());
                        FileUtils.copyDirectory(arg.sdir, copydir);
                        System.out.println("\tDirectory Sucessfuly Copied!");
                    }
                } catch (Exception e) {
                    System.err.println("\tERROR: Could not parse given paths");
                }

            } else {
                System.err.println("\tERROR: Could not parse given paths");
            }
        } else {
            commander.usage();
        }

    }

}
