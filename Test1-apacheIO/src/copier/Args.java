/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package copier;

import com.beust.jcommander.Parameter;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author edd
 */
public class Args {

    @Parameter
    private List<String> parameters = new ArrayList<>();

    @Parameter(names = {"-source", "-s"}, description = "Source file path", converter = FileConverter.class)
    File source;
    @Parameter(names = {"-dest", "-d"}, description = "Destination folder path", converter = FileConverter.class)
    File dest;
    @Parameter(names = {"-help", "-h", "-?"}, description = "Displays help")
    boolean help = false;
    @Parameter(names = {"-repeat", "-r"}, description = "How many times to repeate the process")
    int repeate = 0;
    @Parameter(names = {"-sdir", "-sourcedir"}, description = "Source directory path", converter = FileConverter.class)
    File sdir;
    @Parameter(names = {"-ddir", "-destdir"}, description = "Destination directory path", converter = FileConverter.class)
    File ddir;
}
