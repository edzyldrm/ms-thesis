import argparse
from fsbench import FsBench


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("tasks", metavar='fs_tasks.json', type=str, nargs='+',
                        help='Tasks in json files.')
    parser.add_argument("-r", dest="repeat", type=int, default=1,
                        help='Repeat count.')
    args = parser.parse_args()
    task_files = args.tasks
    repeat_count = args.repeat
    fs_bench = FsBench(repeat_count)
    fs_bench.load_tasks(task_files)
    fs_bench.run()

