import json
import subprocess

from fstask import FsTask
from utils import random_string
from config import hdfs_source_dir, hdfs_dest_dir, fs_source_dir, fs_dest_dir


class FsBench:
    def __init__(self, repeat_count):
        self.hdfs = None
        self.host_fs = None
        self.repeat_count = repeat_count
        self.hdfs_source_base = hdfs_source_dir
        self.hdfs_dest_base = hdfs_dest_dir
        self.fs_source_base = fs_source_dir
        self.fs_dest_base = fs_dest_dir
        self.output_dir = '/output_' + random_string(8)
        self.tasks = []

    def load_tasks(self, task_files):
        for f_name in task_files:
            with open(f_name) as f:
                t = json.load(f)
                self.tasks.extend(t)

    def run(self):
        for i in range(self.repeat_count):
            print("**************************")
            print("## Round({}/{})".format(i + 1, self.repeat_count))
            print("Preparing for tasks")
            init_commands = [['hdfs', 'dfs', '-mkdir', self.hdfs_dest_base + self.output_dir],
                             ['mkdir', self.fs_dest_base + self.output_dir]]
            for cmd in init_commands:
                (ret, out, err) = self.run_cmd('init', cmd)
                if out:
                    print(out.decode('utf-8'))
                if err:
                    print(err.decode('utf-8'))
            print("**************************")
            for task in self.tasks:
                fs_task = FsTask(task, self.hdfs_source_base, self.hdfs_dest_base, self.fs_source_base,
                                 self.fs_dest_base, self.output_dir)
                print("\tTask: {}".format(fs_task.name))
                init_commands, task_commands = fs_task.commands()
                if init_commands:
                    (ret, out, err) = self.run_cmd('init', init_commands)
                    if out:
                        print(out.decode('utf-8'))
                    if err:
                        print(err.decode('utf-8'))
                if task_commands:
                    try:
                        (ret, out, err) = self.run_cmd('task', task_commands)
                        if out:
                            print(out.decode('utf-8'))
                        if err:
                            print(err.decode('utf-8'))
                    except FileNotFoundError as e:
                        print(e)
            print("**************************")
            print("*** Finishing for tasks")
            init_commands = [['hdfs', 'dfs', '-rm', '-R', self.hdfs_dest_base + self.output_dir],
                             ['rm', '-rf', self.fs_dest_base + self.output_dir]]
            for cmd in init_commands:
                (ret, out, err) = self.run_cmd('clean', cmd)
                if out:
                    print(out.decode('utf-8'))
                if err:
                    print(err.decode('utf-8'))
            print("**************************")

    @classmethod
    def run_cmd(cls, label, args_list):
        """
        run linux commands
        """
        # import subprocess
        print('\t {0} : {1}'.format(label, ' '.join(args_list)))
        proc = subprocess.Popen(args_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        s_output, s_err = proc.communicate()
        s_return = proc.returncode
        return s_return, s_output, s_err
