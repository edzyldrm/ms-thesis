import argparse
import subprocess


def initialize_directories():
    """
    :return:
    """
    print("Initializing HDFBench directories in the HDFS.")

    print("Initializing HDFBench directories in the host FS.")


def run_cmd(args_list):
    """
    run linux commands
    """
    # import subprocess
    print('Running system command: {0}'.format(' '.join(args_list)))
    proc = subprocess.Popen(args_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    s_output, s_err = proc.communicate()
    s_return = proc.returncode
    return s_return, s_output, s_err


if __name__ == "__main__":
    # (ret, out, err) = run_cmd(['hdfs', 'dfs', '-ls', '.'])
    # print(out.decode('utf-8'))
    parser = argparse.ArgumentParser()
    parser.add_argument("--tasks", default='', metavar='task.json,task2.json',
                        help='Tasks in json files.')
    args = parser.parse_args()
    if args.init:
        initialize_directories()
    elif args.tasks:
        print(args.tasks)
    else:
        pass
