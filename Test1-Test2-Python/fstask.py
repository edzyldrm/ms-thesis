from utils import random_string


class FsTask:
    def __init__(self, task: dict, hdfs_source_base: str, hdfs_output_base: str,
                 fs_source_base: str, fs_output_base: str, output_dir: str):
        self.hdfs_source_base = hdfs_source_base
        self.hdfs_output_base = hdfs_output_base
        self.fs_source_base = fs_source_base
        self.fs_output_base = fs_output_base
        self.output_dir = output_dir
        self.regex = task.get('regex', "'dfs[a-z.]+'")

        self.name = task.get("name", "Task")
        self.command = task.get("command")

        loc = task.get("source", ":").split(":")
        self.source_fs = loc[0] if len(loc) > 0 else ''
        self.source_name = loc[1] if len(loc) > 1 else ''
        self.hdfs_source_name = self.hdfs_source_base + '/' + self.source_name
        self.fs_source_name = self.fs_source_base + '/' + self.source_name
        loc = task.get("destination", ":").split(":")
        self.destination_fs = loc[0] if len(loc) > 0 else self.source_fs
        self.destination_name = loc[1] if len(loc) > 1 else self.source_name
        self.hdfs_destination_name = self.hdfs_output_base + output_dir + '/' + self.destination_name
        self.fs_destination_name = self.fs_output_base + output_dir + '/' + self.destination_name

        self.cmd_line = task.get("cmd_line")

    def commands(self):
        init_cmd_list, cmd_list = [], []
        if self.command == 'copy':
            init_cmd_list, cmd_list = self.copy_command()
        elif self.command == 'delete':
            init_cmd_list, cmd_list = self.delete_command()
        elif self.command == 'move':
            init_cmd_list, cmd_list = self.move_command()
        elif self.command == 'mapReduce':
            init_cmd_list, cmd_list = self.mapreduce_command()
        elif self.command == 'wordCount':
            init_cmd_list, cmd_list = self.wordcount_command()
        elif self.command == 'wordCountFile':
            init_cmd_list, cmd_list = self.wordcount_file_command()
        elif self.command == 'hive':
            init_cmd_list, cmd_list = self.hive_command()

        return init_cmd_list, cmd_list

    def copy_command(self):
        if self.source_fs == 'hdfs' or self.destination_fs == 'hdfs':
            cmd_list = ['hdfs', 'dfs']
            if self.source_fs == 'hdfs' and self.destination_fs == 'hdfs':
                cmd_list.extend(['-cp', '-f'])
                cmd_list.append(self.hdfs_source_name)
            elif self.source_fs == 'host' and self.destination_fs == 'hdfs':
                cmd_list.extend(['-put', '-f'])
                cmd_list.append(self.fs_source_name)
            cmd_list.append(self.hdfs_destination_name)
        else:
            cmd_list = ['cp', '-r', self.fs_source_name, self.fs_destination_name]

        return [], cmd_list

    def delete_command(self):
        if self.destination_fs == 'hdfs':
            destination_name = self.hdfs_output_base + '/' + self.destination_name
            new_name = destination_name + '_' + random_string(8)
            init_list = ['hdfs', 'dfs', '-cp', destination_name, new_name]
            cmd_list = ['hdfs', 'dfs']
            cmd_list.extend(['-rm', '-R'])
        else:
            destination_name = self.fs_output_base + '/' + self.destination_name
            new_name = destination_name + '_' + random_string(8)
            init_list = ['cp', '-r', destination_name, new_name]
            cmd_list = ['rm', '-rf']

        cmd_list.append(new_name)
        return init_list, cmd_list

    def move_command(self):
        if self.source_fs == 'hdfs' or self.destination_fs == 'hdfs':
            new_name = self.hdfs_source_name + '_' + random_string(8)
            init_list = ['hdfs', 'dfs', '-cp', self.hdfs_source_name, new_name]
            cmd_list = ['hdfs', 'dfs', '-mv', new_name, self.hdfs_destination_name]
        else:
            new_name = self.fs_source_name + '_' + random_string(8)
            init_list = ['cp', '-r', self.fs_source_name, new_name]
            cmd_list = ['mv', new_name, self.fs_destination_name]

        return init_list, cmd_list

    def mapreduce_command(self):
        cmd_list = ['hadoop', 'jar', 'libs/hadoop-mapreduce-2.9.2.jar', 'grep',
                    self.hdfs_source_name, self.hdfs_destination_name, self.regex]
        return [], cmd_list

    def wordcount_command(self):
        cmd_list = ['hadoop', 'jar', 'libs/wc.jar', 'WordCount',
                    self.hdfs_source_name, self.hdfs_destination_name]
        return [], cmd_list

    def wordcount_file_command(self):
        cmd_list = ['java', '-jar', 'libs/wordcountfile.jar',
                    self.fs_source_name]
        return [], cmd_list

    def hive_command(self):
        cmd_list = []
        cmds = self.cmd_line.split()
        for cmd in cmds:
            cmd = cmd.strip()
            if cmd:
                cmd_list.append(cmd)
        return [], cmd_list
